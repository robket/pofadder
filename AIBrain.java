/*
 *      AIBrain.java
 *      
 *      Copyright 2007 Robert Ketteringham <robket@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

import java.util.*;
public class AIBrain implements Runnable
{
	Vector todo;
	int [] [] grid;
	
	public AIBrain(int x, int y)
	{
		todo=new Vector();
		grid = new int [x][y];
	}
	
	public void run ()
	{
		while (true)
		{
		if (!todo.isEmpty())
			process (todo.remove(0));
		try
		{
			Thread.sleep(3);
		}
		catch (InterruptedException ie)
		{
			System.out.println(ie);
		}
		}
	}
	
	public void process (String instruct)
	{
		if (instruct.charAt(0)=='0')
		{
			String [] values = instruct.subString(1).split("@");
			int x=Integer.parseInt(values[0]);
			int y=Integer.parseInt(values[1]);
			int value=Integer.parseInt(values[2]);
			if ()
		}
		else
		{
			String [] values = instruct.subString(1).split("@");
			int x=Integer.parseInt(values[0]);
			int y=Integer.parseInt(values[1]);
			int xFrom=Integer.parseInt(values[2]);
			int yFrom=Integer.parseInt(values[3]);
		}
	}
	
	public void add (int x, int y, int value)
	{
		todo.add("0"+x+"@"+y+"@"+value);
	}
	
	public void add (int x, int y, int xfrom,int yfrom)
	{
		todo.add("1"+x+"@"+y+"@"+xfrom+"@"+yfrom);
	}
	
	public int getAIMove(int x,int y)
	{
	}

}
