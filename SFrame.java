/*
 *      SFrame.java
 *
 *      Copyright 2007 Robert Ketteringham <robket@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

// The "SFrame" class.
import java.awt.event.*;
import javax.swing.*;

public class SFrame extends JFrame implements KeyListener
{
	int[] [] keys;
	int[] [] pushed;
	public SFrame (String name)
	{
		super (name);
		addKeyListener (this);
	}


	public void setListeners (int[] [] keysIn)
	{
		keys = keysIn;
		pushed = new int [keys.length] [3];
	}


	public int getCode (int i)
	{
		if (pushed [i] [1] != 0)
		{
			pushed [i] [0] = pushed [i] [1];
			pushed [i] [1] = pushed [i] [2];
			pushed [i] [2] = 0;
			return pushed [i] [0];
		}
		else
			return 0;
	}


	public void setCode (int i, int codeIn)
	{
		pushed [i] [0] = codeIn;
		pushed [i] [1] = 0;
		pushed [i] [2] = 0;
	}


	public void keyPressed (KeyEvent ke)
	{
		//System.out.println(ke.getKeyCode()+" = " + ke.getKeyText(ke.getKeyCode()) );
		
		for (int i = 0 ; i < keys.length ; i++)
		{
			for (int j = 0 ; j < keys [i].length ; j++)
			{
				if (keys [i] [j] == ke.getKeyCode ())
				{
					if (pushed [i] [1] == 0 & keys [i] [(j + 2) % keys [i].length] != pushed [i] [0])
					{
						pushed [i] [1] = ke.getKeyCode ();
					}

					else if (pushed [i] [1] != 0 & pushed [i] [2] == 0 & keys [i] [(j + 2) % keys [i].length] != pushed [i] [1])
					{
						pushed [i] [2] = ke.getKeyCode ();
					}
					/*
					System.out.println("0:"+ke.getKeyText (pushed [i] [0]));
					System.out.println("1:"+ke.getKeyText (pushed [i] [1]));
					System.out.println("2:"+ke.getKeyText (pushed [i] [2]));
					System.out.println();*/
					break;
				}
			}
		}
	}


	public void keyReleased (KeyEvent ke)
	{
	}


	public void keyTyped (KeyEvent ke)
	{
	}


	/*public int[] directs;
	public int[] [] keys;

	public SFrame (String name, int[] [] keysIn, int[] directsIn)
	{
	super (name);       // Set the frame's name
	addKeyListener (this);
	keys = keysIn;
	directs = directsIn;
	deb (0, "Frame initialised");
	} // Constructor


	public int getX (int i)
	{
	int ret = 0;
	if (directs [i] == 0)
		ret = 1;
	else if (directs [i] == 2)
		ret = -1;
	deb (1, "Get X from frame: " + ret);
	return ret;
	}


	public int getY (int i)
	{
	int ret = 0;
	if (directs [i] == 1)
		ret = 1;
	else if (directs [i] == 3)
		ret = -1;
	deb (1, "Get Y from frame: " + ret);
	return ret;
	}


	public void setDirect (int i, int x, int y)
	{
	if (x == 1)
		directs [i] = 0;
	else if (x == -1)
		directs [i] = 2;
	else if (y == 1)
		directs [i] = 3;
	else if (y == -1)
		directs [i] = 1;
	deb (1, "Set direction: " + directs [i]);
	}


	public void keyPressed (KeyEvent ke)
	{
	deb (3, "Pressed " + ke.getKeyCode ());
	for (int i = 0 ; i < keys.length ; i++)
	{
		for (int j = 0 ; j < keys [i].length ; j++)
		{
		if (keys [i] [j] == ke.getKeyCode () & (directs [i] + j) % 2 == 1)
		{
			directs [i] = j;
		}
		}
	}
	}


	public void keyReleased (KeyEvent ke)
	{
	}


	public void keyTyped (KeyEvent ke)
	{
	}


	public static void deb (int i, String in)
	{/*
	if (i == 0)
		System.out.println ("\t... " + in);
	if (i == 1)
		System.out.println (">>> " + in);
	if (i == 2)
		System.out.println ("\t<<< " + in);
	if (i == 3)
		System.out.println ("!!! " + in);
	}*/
} // SFrame class
