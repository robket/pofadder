/*
 *      Menu.java
 *      
 *      Copyright 2007 Robert Ketteringham <robket@BOB>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

import java.awt.*;
import javax.swing.*;

public class Menu extends JFrame implements keyListener {
	public final int APPEAR=0;
	public final int SCROLL_LEFT=-1;
	public final int SCROLL_RIGHT=1;
	
	public Color bg=Color.white;
	public Color unselected=Color.black;
	public Color selected = Color.red;
	
	public int velocity;
	public int acceleration;
	public int delay; //delay between menu Items
	
	int width;
	int height;
	Container c;
	Option [] options;
	
	public Menu (int widthIn,int heightIn)
	{
		super();
		setSize(widthIn,heightIn);
		width=widthIn;
		height=heightIn;
		setLocationRelativeTo(null);
		setUndecorated(true);
		
		c=getContentPane();
		c.setBackground(bg);
		c.show();
		refreshComponent();
	}
	
	public add(Option [] optionsIn, int moveType)
	{
			option=optionsIn;
			for (int i=0; i<options.length; i++)
			{
				option[i].setForeground(unselected);
				option[i].setLocation((width-option[i].getWidth())/2+moveType*(width+option[i].getWidth())/2);
				option[i].show();
				c.add(option[i]);
			}
	}
	
	public static void refreshComponent ()
    {
	c.setSize ((int) c.getSize ().getWidth () + 1, (int) c.getSize ().getHeight () + 1);
	c.setSize ((int) c.getSize ().getWidth () - 1, (int) c.getSize ().getHeight () - 1);
    }

}
