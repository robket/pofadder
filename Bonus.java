/*
 *      Bonus.java
 *
 *      Copyright 2007 Robert Ketteringham <robket@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


import javax.swing.*;
public class Bonus extends SLabel
{
    int type;
    int lifespan;
    public Bonus (int typeIn, int width, int height, int x, int y, int realX, int realY, int lifespanIn)
    {
	super ();
	setSize (width, height);
	setMyLocation (x, y, realX, realY);
	lifespan = lifespanIn;
	setType (typeIn);
	setPic ();
	deb (5, "Made a new Bonus with expiry date set to: " + lifespan);
    }


    public boolean alive (int date)
    {
	deb (5, "checking... expiry=" + lifespan + "   date=" + date + "   returning: " + (lifespan < date | lifespan == 0));
	return (lifespan > date | lifespan == 0);
    }


    public void setType (int typeIn)
    {
	type = typeIn;
	deb (0, "Set bonus type");
    }


    public int getType ()
    {
	deb (1, "returning bonus type");
	return type;
    }


    public void setPic ()
    {
	setIcon (new ImageIcon ("images/B" + type + ".gif"));
	deb (0, "Set bonus's picture");
    }


    public static void deb (int i, String in)
    {
	/*if (i==5)
	    System.out.println (in);
	if (i==1)
	    System.out.println (">>> "+in);
	if (i==2)
	    System.out.println ("\t<<< "+in);*/
    }
}
