 Usage:

$ javac PofAdder.java
$ java PofAdder

 Configuration Settings:

$ nano pofadder.conf

 Description:
 
PofAdder - A snake game

This game was my Grade 11 IT project. At the time I knew nothing of code style, nor concurrency, etc. etc. I just wanted to make a fun game, and I didn't care if it took me making >1000 line classes to do it!

About the name - Pofadder is the Afrikaans name for Bitis arietans, a venomous viper responsible for the most snakebite fatalities in Africa. Its also the name of a tiny town in the middle of the karoo.
 
 Copyright:

Copyright 2007 Robert Ketteringham <robket@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

You are free to modify, repackage and redistribute all images used by this
program.
