/*
 *      Snake.java
 *
 *      Copyright 2007 Robert Ketteringham <robket@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

import java.util.*;
import java.awt.*;
import javax.swing.*;

public class Snake
{

	Vector sects;
	String snakeName;
	int snakeType;
	int mult;
	int foodLeft;
	int xDirect;
	int yDirect;
	int xOff;
	int yOff;
	int width;
	int height;
	int lives;
	int kills=0;
	int deaths=0;
	int score=0;
	ImageIcon pic;

	public Snake (String snakeNameIn, int multIn,  int xOffIn, int yOffIn,  int widthIn, int heightIn, ImageIcon picIn, int livesIn,int snakeTypeIn)
	{
	sects=new Vector();
	snakeName = snakeNameIn;
	mult = multIn;
	xOff=xOffIn;
	yOff=yOffIn;
	pic = picIn;
	width = widthIn;
	height = heightIn;
	lives=livesIn;
	snakeType=snakeTypeIn;
	}
	
	public SLabel spawn (int startXPos, int startYPos, int startXDir, int startYDir ,int startLength)
	{
	//System.out.println("Set: "+snakeNum);
	setFoodLeft (startLength);
	setDir (startXDir, startYDir);
	SLabel temp = new SLabel (pic);
	temp.setSize (mult, mult);
	temp.setMyLocation (startXPos,startYPos,startXPos * mult+xOff, startYPos * mult+yOff);
	temp.show ();
	sects.add (temp);
	return ((SLabel) sects.get (sects.size() - 1));
	}


	public void addFood (int foodLeftIn)
	{
	foodLeft = foodLeft + foodLeftIn;
	}


	public void setFoodLeft (int foodLeftIn)
	{
	foodLeft = foodLeftIn;
	}
	
	public int getFoodLeft ()
	{
	return foodLeft;
	}
	
	public void addLives (int livs)
	{
	lives=lives+livs;
	}
	
	public void setLives (int livs)
	{
	lives=livs;
	}
	
	public int getLives ()
	{
	return lives;
	}
	
	public void addKills (int kils)
	{
	kills=kills+kils;
	}
	
	public void setKills (int kils)
	{
	kills=kils;
	}
	
	public int getKills ()
	{
	return kills;
	}
	
	public void addDeaths (int death)
	{
	deaths=deaths+death;
	}
	
	public void setDeaths (int death)
	{
	deaths=death;
	}
	
	public int getDeaths ()
	{
	return deaths;
	}

	public void addScore (int scor)
	{
	score=score+scor;
	}
	
	public void setScore (int scor)
	{
	score=scor;
	}
	
	public int getScore ()
	{
	return score;
	}
	
	public int getLength()
	{
	    return sects.size();
	}
	
	public void setDir (int x, int y)
	{
	xDirect = x;
	yDirect = y;
	deb(0,"Set snakes direction. X: "+x+" Y: "+y);
	}
	
	public int getXDir ()
	{
	return xDirect;
	}
	public int getYDir ()
	{
	return yDirect;
	}
	
	public SLabel grow ()
	{
	foodLeft--;
	SLabel temp = new SLabel (pic);
	temp.setSize (mult, mult);
	int [] temp2=nextMove();
	temp.setMyLocation (temp2[0],temp2[1],temp2[0]*mult + xOff,temp2[1]*mult+yOff);
	temp.show ();
	sects.insertElementAt (temp, 0);
	deb(0,"Grew snake");
	return ((SLabel) sects.get (0));
	}


	public void move ()
	{
	int [] temp=nextMove();
	((SLabel) sects.get (sects.size () - 1)).setMyLocation (temp[0],temp[1],temp[0]*mult + xOff,temp[1]*mult+yOff);
	sects.insertElementAt (sects.remove (sects.size () - 1), 0);
	deb(0,"Moved snake");
	}
	public void relocate(int x, int y)
	{
	((SLabel) sects.get (sects.size () - 1)).setMyLocation (x,y,x*mult + xOff,y*mult+yOff);
	sects.insertElementAt (sects.remove (sects.size () - 1), 0);
	deb(0,"Relocated ");
	}


	public boolean empty ()
	{
	deb(2,"Empty: "+sects.isEmpty());
	return sects.isEmpty ();
	}


	public SLabel remove ()
	{
	deb(0,"removing");
	return ((SLabel) sects.remove (sects.size () - 1));
	}
	
	public Vector getArray()
	{
	return (sects);
	}
	
	public void removeAll()
	{
	sects.removeAllElements();
	}


	public boolean isFoodLeft ()
	{
	deb(0,"Food left:"+(foodLeft>0));
	return foodLeft > 0;
	}
	
	public int getType()
	{
		return snakeType;
	}
	
	public String getName()
	{
	    return snakeName;
	}


	public int[] nextMove ()
	{
	//System.out.println(snakeNum+" : "+ sects.size());
	int [] ret={((int) ((SLabel) sects.get (0)).getMyLocation ().getX () + xDirect+ width) % (width), ((int) ((SLabel) sects.get (0)).getMyLocation ().getY () + yDirect + height) % (height)};
	return ret;
	}


	public boolean getSnake (int x, int y)
	{
	boolean ret = false;
	for (int i = 0 ; i < sects.size () ; i++)
	{
		if ((int) ((SLabel) sects.get (i)).getMyLocation ().getX () == x & (int) ((SLabel) sects.get (i)).getMyLocation ().getY () == y)
		{
		ret = true;
		break;
		}
	}
	deb(2,""+ret);
	return ret;
	}
	
	public static void deb (int i, String in)
	{
	/*if (i==0)
		System.out.println ("\t... "+in);
	if (i==1)
		System.out.println (">>> "+in);
	if (i==2)
		System.out.println ("\t<<< "+in);*/
	}
}
