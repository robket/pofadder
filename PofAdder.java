/*
 *      PofAdder.java
 *
 *      Copyright 2007 Robert Ketteringham <robket@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*             Still to do
 *
 *             Make a proper menu
 *             Make everything look prettier
 *             Add AI
 *             Add networking
 *             Add comments (eg. "Snake 1 is owning", "Triple kill!")
 *                         Add chat
 *             Add sound
 *             Make a map editor
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

class PofAdder
{
    //To do with game
    static final double version = 0.8;
    static boolean roundContinues = true;
    static SFrame sFrame;
    static Container c;
    static boolean showSplash = true;
    static int speed = 200; //Determined by menu
    static int totalDeaths = 0;
    //To do with level/frame
    static String level = "none"; //determined by menu
    static int frameWidth = 80; //Determined by level
    static int frameHeight = 60; //Determined by level
    static JLabel[] borders;
    static JLabel[] info;
    static int mult = 10; //Not really changeable
    static int xOff = 5;
    static int yOff = 20;
    static Color backCol = new Color (255, 255, 255);
    static Color foreCol =     new Color (66, 66, 66);
    static Color selCol = Color.blue;
    //To do with snakes
    static int humanSnakes = 2; //Set by menu
    static int aiSnakes = 0; //Also set by menu
    static Vector boni = new Vector ();
    static Snake[] snakes; //Size determined by menu
    static int[] [] keys = {{27}, {39, 40, 37, 38}, {68, 83, 65, 87}, {78, 66, 86, 71}, {99, 98, 97, 101}}; //Menu determines this
    static Color[] darks;
    static Color[] lights;
    static int startLength = 10;
    //To do with AI
    static int[] bonValue = {5, 0, 1, 0, 3, 7, 8, 4, 0, 10, 0, 4, 0, 6};
    //static AIBrain aiBrain;
    //To do with bonuses
    static int time = 0;
    static String[] bonName = {"Food", "Wall", "Speed Up", "Slow Down", "Extra Life", "Bonus Points", "Evergrow", "Swaztika", "Halve Points", "Nuke Others", "Normalise", "BFGS!", "Diet", "Earthquake"};
    static int[] bonRarity = {10001, 501, 701, 701, 999, 501, 801, 401, 1001, 1201, 399, 1201, 901, 2101}; //Max 10000
    static int[] bonExists = {0, 1000, 300, 500, 250, 350, 200, 200, 150, 300, 400, 500, 400, 200};
    static int[] bonScore = {30, -10, 20, -20, 50, 100, 40, 75, 0, 100, 0, 200, 40, -50};
    static int defaultLives = 5; //Menu again
    static int foodNut = 5;

    // ############################################

    public static void main (String[] args)
    {
    loadSettings ();
    if (showSplash)
        splash ();
    //menu();
    loadLevel ();
    addSnakes ();
    addBonus (0, 0);
    //aiBrain=new aiBrain();
    //new Thread (aiBrain).start();
    go ();
    System.exit (0);
    }


    public static void go ()
    {
    while (sFrame.getCode (0) != 27)
    {
        for (int i = 0 ; i < snakes.length ; i++)
        {
        int temp = 0;
        if (snakes [i].getType () == 0)
        {
            temp = sFrame.getCode (i + 1);
            if (temp == keys [i + 1] [0])
            snakes [i].setDir (1, 0);
            else if (temp == keys [i + 1] [1])
            snakes [i].setDir (0, 1);
            else if (temp == keys [i + 1] [2])
            snakes [i].setDir (-1, 0);
            else if (temp == keys [i + 1] [3])
            snakes [i].setDir (0, -1);
        }
        else
        {
            temp = getAIMove (i);
            if (temp == 0)
            snakes [i].setDir (1, 0);
            else if (temp == 1)
            snakes [i].setDir (-1, 0);
            else if (temp == 2)
            snakes [i].setDir (0, 1);
            else if (temp == 3)
            snakes [i].setDir (0, -1);
        }
        if (getSnake (snakes [i].nextMove () [0], snakes [i].nextMove () [1]) >= 0)
        {
            doSnake (i, getSnake (snakes [i].nextMove () [0], snakes [i].nextMove () [1]));
        }
        else
        {
            temp = getBonus (snakes [i].nextMove () [0], snakes [i].nextMove () [1]);
            doBonus (temp, i);

        }
        if (snakes [i].isFoodLeft ())
        {
            c.add (snakes [i].grow ());
        }
        else
        {
            snakes [i].move ();
        }
        }
        randomSpawn ();
        checkLife ();
        refreshInfo ();
        refreshComponent ();
        time++;
        try
        {
        Thread.sleep (speed);
        }
        catch (InterruptedException ie)
        {
        System.out.println (ie);
        }
    }
    }


    // ############################################

    //      AI STUFF

    /*public static void makeGrid ()
    {
    }

    public static int updateGrid(int x, int y) //DEPTH SEARCH - will result in very VERY deep recursion.
    {
        int after=grid[x][y];
        after=compareGrid(after,grid[(x+frameWidth+1)%frameWidth][(y+frameHeight)%frameHeight]);
        after=compareGrid(after,grid[(x+frameWidth-1)%frameWidth][(y+frameHeight)%frameHeight]);
        after=compareGrid(after,grid[(x+frameWidth)%frameWidth][(y+frameHeight+1)%frameHeight]);
        after=compareGrid(after,grid[(x+frameWidth)%frameWidth][(y+frameHeight-1)%frameHeight]);
        if (after!=grid[x][y])
        {
            grid[x][y]=after;
            updateGrid((x+frameWidth+1)%frameWidth,(y+frameHeight)%frameHeight);
            updateGrid((x+frameWidth-1)%frameWidth,(y+frameHeight)%frameHeight);
            updateGrid((x+frameWidth)%frameWidth,(y+frameHeight+1)%frameHeight);
            updateGrid((x+frameWidth)%frameWidth,(y+frameHeight-1)%frameHeight);
        }
    }

    public static int compareGrid(int toChange, int toCompare)
    {
        int ret=toChange;
    }*/

    public static int getAIMove (int sna)
    {
    return (int) (Math.random () * 40);
    }


    //      DRAWING STUFF

    public static void initFrame ()
    {
    sFrame = new SFrame ("Snake");
    yOff = 35 + 15 * (humanSnakes + aiSnakes);

    sFrame.setSize (frameWidth * mult + xOff + 5, frameHeight * mult + yOff + 5);
    sFrame.setLocationRelativeTo (null);
    sFrame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    sFrame.setUndecorated (true);
    sFrame.show ();

    c = sFrame.getContentPane ();
    c.setLayout (null);
    c.setBackground (backCol);
    c.show ();
    }


    public static void drawFrame ()
    {
    info = new JLabel [3 + (humanSnakes + aiSnakes) * 7];

    info [0] = new JLabel ();
    info [0].setSize (200, 15);
    info [0].setLocation (10, 10);
    info [0].setFont (new Font ("None", Font.BOLD, 20));
    info [0].setForeground (selCol);
    info [0].show ();
    c.add (info [0]);

    info [1] = new JLabel ();
    info [1].setSize (150, 15);
    info [1].setLocation (230, 10);
    info [1].setFont (new Font ("None", Font.BOLD, 20));
    info [1].setForeground (selCol);
    info [1].show ();
    c.add (info [1]);

    info [2] = new JLabel ();
    info [2].setSize (150, 15);
    info [2].setLocation (380, 10);
    info [2].setFont (new Font ("None", Font.BOLD, 20));
    info [2].setForeground (selCol);
    info [2].show ();
    c.add (info [2]);

    for (int i = 0 ; i < aiSnakes + humanSnakes ; i++)
    {

        info [i * 7 + 3 + 0] = new JLabel ();
        //info [i * 7 + 3 + 0].setText ("SNAKE " + (i + 1));
        info [i * 7 + 3 + 0].setSize (90, 10);
        info [i * 7 + 3 + 0].setLocation (10, i * 15 + 32);
        info [i * 7 + 3 + 0].setFont (new Font ("None", Font.BOLD, 12));
        info [i * 7 + 3 + 0].setForeground (darks [i]);
        info [i * 7 + 3 + 0].show ();
        c.add (info [i * 7 + 3 + 0]);

        info [i * 7 + 3 + 1] = new JLabel ();
        //info [i * 7 + 3 + 1].setText ("GROWTH: ");
        info [i * 7 + 3 + 1].setSize (90, 10);
        info [i * 7 + 3 + 1].setLocation (100, i * 15 + 32);
        info [i * 7 + 3 + 1].setFont (new Font ("None", Font.BOLD, 12));
        info [i * 7 + 3 + 1].setForeground (darks [i]);
        info [i * 7 + 3 + 1].show ();
        c.add (info [i * 7 + 3 + 1]);

        info [i * 7 + 3 + 2] = new JLabel ();
        //info [i * 7 + 3 + 2].setText ("LENGTH: " + snakes [i].getLength ());
        info [i * 7 + 3 + 2].setSize (90, 10);
        info [i * 7 + 3 + 2].setLocation (200, i * 15 + 32);
        info [i * 7 + 3 + 2].setFont (new Font ("None", Font.BOLD, 12));
        info [i * 7 + 3 + 2].setForeground (darks [i]);
        info [i * 7 + 3 + 2].show ();
        c.add (info [i * 7 + 3 + 2]);

        info [i * 7 + 3 + 3] = new JLabel ();
        //info [i * 7 + 3 + 3].setText ("DEATHS: " + snakes [i].getDeaths ());
        info [i * 7 + 3 + 3].setSize (90, 10);
        info [i * 7 + 3 + 3].setLocation (300, i * 15 + 32);
        info [i * 7 + 3 + 3].setFont (new Font ("None", Font.BOLD, 12));
        info [i * 7 + 3 + 3].setForeground (darks [i]);
        info [i * 7 + 3 + 3].show ();
        c.add (info [i * 7 + 3 + 3]);

        info [i * 7 + 3 + 4] = new JLabel ();
        //info [i * 7 + 3 + 4].setText ("KILLS: " + snakes [i].getKills ());
        info [i * 7 + 3 + 4].setSize (90, 10);
        info [i * 7 + 3 + 4].setLocation (400, i * 15 + 32);
        info [i * 7 + 3 + 4].setFont (new Font ("None", Font.BOLD, 12));
        info [i * 7 + 3 + 4].setForeground (darks [i]);
        info [i * 7 + 3 + 4].show ();
        c.add (info [i * 7 + 3 + 4]);

        info [i * 7 + 3 + 5] = new JLabel ();
        //info [i * 7 + 3 + 5].setText ("SCORE: " + snakes [i].getScore ());
        info [i * 7 + 3 + 5].setSize (90, 10);
        info [i * 7 + 3 + 5].setLocation (500, i * 15 + 32);
        info [i * 7 + 3 + 5].setFont (new Font ("None", Font.BOLD, 12));
        info [i * 7 + 3 + 5].setForeground (darks [i]);
        info [i * 7 + 3 + 5].show ();
        c.add (info [i * 7 + 3 + 5]);

        info [i * 7 + 3 + 6] = new JLabel ();
        //info [i * 7 + 3 + 6].setText ("SNAKE " + (i + 1));
        info [i * 7 + 3 + 6].setSize (frameWidth * mult, 15);
        info [i * 7 + 3 + 6].setLocation (5, i * 15 + 30);
        info [i * 7 + 3 + 6].setBackground (lights [i]);
        info [i * 7 + 3 + 6].setOpaque (true);
        info [i * 7 + 3 + 6].show ();
        c.add (info [i * 7 + 3 + 6]);
    }

    borders = new JLabel [5];
    borders [0] = new JLabel ();
    borders [0].setSize (frameWidth * mult, 5);
    borders [0].setLocation (xOff, 0);
    borders [0].setBackground (foreCol);
    borders [0].setForeground (foreCol);
    borders [0].setOpaque (true);
    borders [0].show ();
    c.add (borders [0]);

    borders [1] = new JLabel ();
    borders [1].setSize (frameWidth * mult, 5);
    borders [1].setLocation (xOff, yOff - 5);
    borders [1].setBackground (foreCol);
    borders [1].setForeground (foreCol);
    borders [1].setOpaque (true);
    borders [1].show ();
    c.add (borders [1]);

    borders [2] = new JLabel ();
    borders [2].setSize (5, frameHeight * mult + yOff);
    borders [2].setLocation (xOff - 5, 0);
    borders [2].setBackground (foreCol);
    borders [2].setForeground (foreCol);
    borders [2].setOpaque (true);
    borders [2].show ();
    c.add (borders [2]);

    borders [3] = new JLabel ();
    borders [3].setSize (5, frameHeight * mult + yOff);
    borders [3].setLocation (frameWidth * mult + xOff, 0);
    borders [3].setBackground (foreCol);
    borders [3].setForeground (foreCol);
    borders [3].setOpaque (true);
    borders [3].show ();
    c.add (borders [3]);

    borders [4] = new JLabel ();
    borders [4].setSize (frameWidth * mult + 10, 5);
    borders [4].setLocation (xOff - 5, frameHeight * mult + yOff);
    borders [4].setBackground (foreCol);
    borders [4].setForeground (foreCol);
    borders [4].setOpaque (true);
    borders [4].show ();
    c.add (borders [4]);
    refreshComponent ();
    }


    public static void splash ()
    {
    JFrame j = new JFrame ();
    j.setSize (600, 600);
    j.setLocationRelativeTo (null);
    j.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    j.setUndecorated (true);

    Container jc = j.getContentPane ();
    jc.setLayout (null);
    jc.setBackground (backCol);

    JLabel vers = new JLabel ();
    vers.setText ("Version " + version);
    vers.setSize (150, 15);
    vers.setLocation (225, 575);
    vers.setHorizontalAlignment (JLabel.CENTER);
    vers.setForeground (selCol);
    vers.setFont (new Font ("None", Font.BOLD, 20));
    vers.show ();
    jc.add (vers);

    JLabel backDrop = new JLabel ();
    backDrop.setSize (600, 23);
    backDrop.setLocation (0, 571);
    backDrop.setBackground (foreCol);
    backDrop.setOpaque (true);
    backDrop.show ();
    jc.add (backDrop);

    JLabel pretty = new JLabel (new ImageIcon ("images/StupidSplash.jpg"));
    pretty.setSize (600, 600);
    pretty.setLocation (0, 0);
    pretty.show ();
    jc.add (pretty);


    jc.setSize ((int) jc.getSize ().getWidth () + 1, (int) jc.getSize ().getHeight () + 1);
    jc.setSize ((int) jc.getSize ().getWidth () - 1, (int) jc.getSize ().getHeight () - 1);

    jc.show ();
    j.show ();
    try
    {
        Thread.sleep (1000);
    }


    catch (InterruptedException ie)
    {
        System.out.println (ie);
    }


    j.dispose ();
    }


    //      LOADING STUFF

    public static void loadLevel ()
    {
    //I may have to resize the frame
    if (new File (level).exists ())
    {
        try
        {
        BufferedReader fi = new BufferedReader (new FileReader (level));
        frameWidth = Integer.parseInt (fi.readLine ());
        frameHeight = Integer.parseInt (fi.readLine ());
        initFrame ();
        drawFrame ();
        int temp = Integer.parseInt (fi.readLine ());
        for (int i = 0 ; i < temp ; i++)
        {
            addBonus (Integer.parseInt (fi.readLine ()), Integer.parseInt (fi.readLine ()), Integer.parseInt (fi.readLine ()), Integer.parseInt (fi.readLine ()));
        }
        fi.close ();
        }
        catch (IOException ioe)
        {
        System.out.println (ioe);
        }
    }
    else
    {
        initFrame ();
        drawFrame ();
    }
    sFrame.setListeners (keys);
    }


    public static void loadSettings ()
    {
    BufferedReader fi;
    try
    {
        fi = new BufferedReader (new FileReader ("pofadder.conf"));
        while (fi.ready ())
        {
        String in = fi.readLine ();
        String attrib = "";
        String value = "";
        if (in.charAt (0) != '#')
        {
            attrib = in.substring (0, in.indexOf (":"));
            attrib = attrib.trim ();
            value = in.substring (in.indexOf (":") + 1);
            if (value.indexOf ("#") != -1)
            value = value.substring (0, value.indexOf ("#"));
            value.trim ();
            changeAttrib (attrib, value);
        }
        }
        fi.close ();

    }
    catch (FileNotFoundException fnfe)
    {
        saveSettings ("No settings file exists... do you want to create one?");
    }
    catch (Exception e)
    {
        saveSettings ("Woops, something went wrong. Heres the error:\n" + e + "\n\n Do you want to create a new setiings file?");
    }
    }


    public static void changeAttrib (String attrib, String value)
    {
    if (attrib.equalsIgnoreCase ("Speed"))
        speed = Integer.parseInt (value);
    else if (attrib.equalsIgnoreCase ("Level"))
        level = value;
    else if (attrib.equalsIgnoreCase ("DefaultWidth"))
        frameWidth = Integer.parseInt (value);
    else if (attrib.equalsIgnoreCase ("DefaultHeight"))
        frameHeight = Integer.parseInt (value);
    else if (attrib.equalsIgnoreCase("HumanSnakes"))
        humanSnakes = Integer.parseInt(value);
    else if (attrib.equalsIgnoreCase("AISnakes"))
        aiSnakes = Integer.parseInt(value);
    else if (attrib.equalsIgnoreCase ("StartLength"))
        startLength = Integer.parseInt (value);
    else if (attrib.equalsIgnoreCase ("FoodNut"))
        foodNut = Integer.parseInt (value);
    else if (attrib.equalsIgnoreCase ("ShowSplash"))
        showSplash = value.equalsIgnoreCase ("True");
    else if (attrib.equalsIgnoreCase ("Keys"))
    {
        String[] players = value.split ("@");
        keys = new int [players.length] [];
        for (int i = 0 ; i < players.length ; i++)
        {
        String[] keyCodes = players [i].split (",");
        keys [i] = new int [keyCodes.length];
        for (int j = 0 ; j < keyCodes.length ; j++)
            keys [i] [j] = Integer.parseInt (keyCodes [j]);
        }
    }
    else if (attrib.equalsIgnoreCase ("Darks"))
    {
        String[] snaks = value.split ("@");
        darks = new Color [snaks.length];
        for (int i = 0 ; i < snaks.length ; i++)
        {
        String[] rgb = snaks [i].split (",");
        darks [i] = new Color (Integer.parseInt (rgb [0]), Integer.parseInt (rgb [1]), Integer.parseInt (rgb [2]));
        }
    }
    else if (attrib.equalsIgnoreCase ("Lights"))
    {
        String[] snaks = value.split ("@");
        lights = new Color [snaks.length];
        for (int i = 0 ; i < snaks.length ; i++)
        {
        String[] rgb = snaks [i].split (",");
        lights [i] = new Color (Integer.parseInt (rgb [0]), Integer.parseInt (rgb [1]), Integer.parseInt (rgb [2]));
        }
    }
    }


    public static void saveSettings (String error)
    {
    if (JOptionPane.showConfirmDialog (null, error, "Save settings?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
    {
        try
        {
        PrintWriter fo = new PrintWriter (new FileWriter ("pofadder.conf"));
        fo.println ("#Automatically saved - edit manually with caution (Although actually nothing serious can happen)");
        fo.println ("Speed:" + speed);
        fo.println ("Level:" + level);
        fo.println ("DefaultWidth:" + frameWidth);
        fo.println ("DefaultHeight:" + frameHeight);
        fo.println ("StartLength:" + startLength);
        fo.println ("FoodNut:" + foodNut);
        fo.println ("ShowSplash:" + showSplash);
        fo.print ("Keys:");
        for (int i = 0 ; i < keys.length ; i++)
        {
            for (int j = 0 ; j < keys [i].length ; j++)
            {
            fo.print ("" + keys [i] [j]);
            if (j < keys [i].length - 1)
                fo.print (",");
            }
            if (i < keys.length - 1)
            fo.print ("@");
        }
        fo.println ();
        fo.print ("Darks:");
        for (int i = 0 ; i < darks.length ; i++)
        {
            fo.print (darks [i].getRed () + "," + darks [i].getGreen () + "," + darks [i].getBlue ());
            if (i < darks.length - 1)
            fo.print ("@");
        }
        fo.println ();
        fo.print ("Lights:");
        for (int i = 0 ; i < lights.length ; i++)
        {
            fo.print (lights [i].getRed () + "," + lights [i].getGreen () + "," + lights [i].getBlue ());
            if (i < lights.length - 1)
            fo.print ("@");
        }
        fo.close ();
        }
        catch (IOException ioe)
        {
        System.out.println (ioe);
        }
    }
    }


    //      GAME STUFF >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    //      SNAKE STUFF

    public static void addSnakes ()
    {
    snakes = new Snake [humanSnakes + aiSnakes];
    for (int i = 0 ; i < humanSnakes ; i++)
    {
        snakes [i] = new Snake ("Snake " + (i+1), mult, xOff, yOff, frameWidth, frameHeight, new ImageIcon ("images/SH" + i + ".gif"), defaultLives, 0);
        spawnSnake (i);
    }
    for (int i = humanSnakes ; i < humanSnakes + aiSnakes ; i++)
    {
        snakes [i] = new Snake ("Snake " + i, mult, xOff, yOff, frameWidth, frameHeight, new ImageIcon ("images/SA" + i + ".gif"), defaultLives, 1);
        spawnSnake (i);
    }
    refreshComponent ();
    }


    public static void spawnSnake (int sna)
    {
    int x;
    int y;
    int dir;
    do
    {
        x = (int) (Math.random () * frameWidth);
        y = (int) (Math.random () * frameHeight);
        dir = (int) (Math.random () * 4);
    }
    while (getSnake (x, y) >= 0 | getBonus (x, y) > 0);
    int dirX = 0;
    int dirY = 0;
    if (dir == 0)
        dirX = 1;
    else if (dir == 1)
        dirY = 1;
    else if (dir == 2)
        dirX = -1;
    else if (dir == 3)
        dirY = -1;
    c.add (snakes [sna].spawn (x, y, dirX, dirY, startLength));
    sFrame.setCode (sna + 1, keys [sna + 1] [dir]);
    }


    public static void killSnake (int sna)
    {
    Vector toDie = snakes [sna].getArray ();
    for (int i = 0 ; i < toDie.size () ; i++)
    {
        c.remove ((SLabel) toDie.get (i));
    }
    snakes [sna].removeAll ();
    spawnSnake (sna);
    refreshComponent ();

    }


    public static int getSnake (int x, int y)
    {
    int ret = -1;
    for (int i = 0 ; i < snakes.length ; i++)
    {
        if (snakes [i] != null)
        {
        if (snakes [i].getSnake (x, y))
        {
            ret = i;
            break;
        }
        }
    }
    return ret;
    }


    public static void doSnake (int killed, int killer)
    {
    killSnake (killed);
    snakes [killed].addLives (-1);
    snakes [killed].addDeaths (1);
    totalDeaths++;
    if (killer == killed)
    {
        snakes [killed].addScore (-50);
    }
    else
    {
        snakes [killed].addScore (-50);
        snakes [killer].addScore (50);
        snakes [killer].addKills (1);
    }
    }


    //      BONUS STUFF

    public static void addBonus (int type, int lifespan)
    {
    int x;
    int y;
    do
    {
        x = (int) (Math.random () * frameWidth);
        y = (int) (Math.random () * frameHeight);
    }


    while (getSnake (x, y) >= 0 | getBonus (x, y) > 0);
    boni.add (new Bonus (type, mult, mult, x, y, x * mult + xOff, y * mult + yOff, time + lifespan));
    c.add (((Bonus) boni.get (boni.size () - 1)));
    refreshComponent ();
    }


    public static void addBonus (int type, int x, int y, int lifespan)
    {
    boni.add (new Bonus (type, mult, mult, x, y, x * mult + xOff, y * mult + yOff, time + lifespan));
    c.add (((Bonus) boni.get (boni.size () - 1)));
    refreshComponent ();
    }


    public static void moveBonus (int i)
    {
    int x;
    int y;
    do
    {
        x = (int) (Math.random () * frameWidth);
        y = (int) (Math.random () * frameHeight);
    }


    while (getSnake (x, y) >= 0 | getBonus (x, y) > 0);
    ((Bonus) boni.get (i)).setMyLocation (x, y, x * mult + xOff, y * mult + yOff);
    refreshComponent ();
    }


    public static void moveBonus (int i, int x, int y)
    {
    ((Bonus) boni.get (i)).setMyLocation (x, y, x * mult + xOff, y * mult + yOff);
    refreshComponent ();
    }


    public static void killBonus (int i)
    {
    c.remove (((Bonus) boni.get (i)));
    boni.remove (i);
    refreshComponent ();
    }


    public static int getBonus (int x, int y)
    {
    int ret = -1;
    for (int i = 0 ; i < boni.size () ; i++)
    {
        if ((int) ((Bonus) boni.get (i)).getMyLocation ().getX () == x & (int) ((Bonus) boni.get (i)).getMyLocation ().getY () == y)
        {
        ret = i;
        break;
        }
    }
    return ret;
    }


    public static void doBonus (int bon, int sna)
    {
    if (bon >= 0)
    {
        if (((Bonus) boni.get (bon)).getType () == 0)
        doFood (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 1)
        doWall (sna);
        else if (((Bonus) boni.get (bon)).getType () == 2)
        doFast (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 3)
        doSlow (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 4)
        doLifeBooster (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 5)
        doScoreBooster (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 6)
        doCreatine (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 7)
        doCrazy (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 8)
        doStop (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 9)
        doNuke (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 10)
        doNormalise (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 11)
        doBFGs (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 12)
        doDiet (bon, sna);
        else if (((Bonus) boni.get (bon)).getType () == 13)
        doEarthquake (bon, sna);
    }
    }


    //  vvv LIST OF BONUSES AND THERE ACTIONS vvv
    public static void doFood (int bon, int sna)
    {
    moveBonus (bon);
    foodNut++;
    snakes [sna].addFood (foodNut);
    snakes [sna].addScore (bonScore [0]);
    }


    public static void doWall (int sna)
    {
    killSnake (sna);
    snakes [sna].addLives (-1);
    snakes [sna].addDeaths (1);
    totalDeaths++;
    snakes [sna].addScore (bonScore [1]);
    }


    public static void doFast (int bon, int sna)
    {
    moveBonus (bon);
    speed = (int) speed / 2;
    snakes [sna].addScore (bonScore [2]);
    }


    public static void doSlow (int bon, int sna)
    {
    moveBonus (bon);
    speed = speed * 2;
    snakes [sna].addScore (bonScore [3]);
    }


    public static void doLifeBooster (int bon, int sna)
    {
    killBonus (bon);
    snakes [sna].addLives (1);
    snakes [sna].addScore (bonScore [4]);
    }


    public static void doScoreBooster (int bon, int sna)
    {
    killBonus (bon);
    snakes [sna].addScore (bonScore [5]);
    }


    public static void doCreatine (int bon, int sna)
    {
    killBonus (bon);
    snakes [sna].addFood (500);
    snakes [sna].addScore (bonScore [6]);
    }


    public static void doCrazy (int bon, int sna)
    {
    killBonus (bon);
    snakes [sna].setDir (5 - (int) (Math.random () * 11), 5 - (int) (Math.random () * 11));
    snakes [sna].addScore (bonScore [7]);
    sFrame.setCode (sna + 1, 0); //EXPERIMENTAL
    }


    public static void doStop (int bon, int sna)
    {
    killBonus (bon);
    snakes [sna].setFoodLeft (0);
    snakes [sna].setScore (snakes [sna].getScore () / 2);
    snakes [sna].addScore (bonScore [8]);
    }


    public static void doNuke (int bon, int sna)
    {
    killBonus (bon);
    for (int i = 0 ; i < snakes.length ; i++)
    {
        if (i != sna)
        {
        doSnake (i, sna);
        }
    }
    snakes [sna].addScore (bonScore [9]);
    }


    public static void doNormalise (int bon, int sna)
    {
    killBonus (bon);
    speed = 40;
    for (int i = 0 ; i < snakes.length ; i++)
        snakes [i].setFoodLeft (0);
    snakes [sna].addScore (bonScore [10]);
    }


    public static void doBFGs (int bon, int sna)
    {
    killBonus (bon);
    for (int i = 0 ; i < snakes.length ; i++)
    {
        if (i != sna)
        snakes [i].addFood (200);

    }
    snakes [sna].addScore (bonScore [11]);
    }


    public static void doDiet (int bon, int sna)
    {
    killBonus (bon);
    snakes [sna].setFoodLeft (-500);
    snakes [sna].addScore (bonScore [12]);
    }


    
    public static void doEarthquake (int bon, int sna)
    {
    killBonus (bon);
    for (int i = 0 ; i < snakes.length ; i++)
    {

        if (i != sna)
        {
        snakes [i].setDir (3 - (int) (Math.random () * 7), 3 - (int) (Math.random () * 7));
        sFrame.setCode (i + 1, 0); //EXPERIMENTAL
        }
    }
    snakes [sna].addScore (bonScore [13]);
    }
    
    // ^^^ INSERT NEW BONUSES HERE ^^^

    //      vvv REPETETIVE THINGS THAT CHECK SOMETHING vvv


    public static void randomSpawn ()
    {
    int spawn = (int) (Math.random () * 10000);
    for (int i = 0 ; i < bonRarity.length ; i++)
    {
        if (spawn % bonRarity [i] == 0)
        addBonus (i, bonExists [i]);
    }
    }


    public static void checkLife ()
    {
    for (int i = 0 ; i < boni.size () ; i++)
    {
        if (!((Bonus) boni.get (i)).alive (time))
        killBonus (i);
    }
    }


    public static void refreshInfo ()
    {
    info [0].setText ("DURATION: " + time);
    info [1].setText ("SPEED: " + (int) (2000 / speed) + "%");
    info [2].setText ("DEATHS: " + totalDeaths);
    for (int i = 0 ; i < aiSnakes + humanSnakes ; i++)
    {
        info [i * 7 + 3 + 0].setText (snakes [i].getName ());
        info [i * 7 + 3 + 1].setText ("GROWTH: " + snakes [i].getFoodLeft ());
        info [i * 7 + 3 + 2].setText ("LENGTH: " + snakes [i].getLength ());
        info [i * 7 + 3 + 3].setText ("DEATHS: " + snakes [i].getDeaths ());
        info [i * 7 + 3 + 4].setText ("KILLS: " + snakes [i].getKills ());
        info [i * 7 + 3 + 5].setText ("SCORE: " + snakes [i].getScore ());
    }
    }


    public static void refreshComponent ()
    {
    c.setSize ((int) c.getSize ().getWidth () + 1, (int) c.getSize ().getHeight () + 1);
    c.setSize ((int) c.getSize ().getWidth () - 1, (int) c.getSize ().getHeight () - 1);
    }
}
